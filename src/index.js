import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import combineReducers from './components/reducers/combineReducers';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { saveToSessionStorage, loadFromSessionStorage } from './functions';
import { userLoggedIn } from './components/actions/auth'

const store = createStore(combineReducers, { cartReducer: loadFromSessionStorage() }, composeWithDevTools(applyMiddleware(thunk)));
store.subscribe(() => saveToSessionStorage(store.getState().cartReducer));

if (localStorage.user) {
  console.log(store.getState().userReducer);
  const user = JSON.parse(localStorage.user);
  console.log(user);
  store.dispatch(userLoggedIn(user));
}

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'));
