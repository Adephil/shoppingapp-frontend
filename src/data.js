const products = [{
    id: 1,
    name: 'Super fun food1',
    description: `Lorem ipsum dolor sit amet consectetur, adipisicing elit. Beatae
     quibusdam nisi alias laboriosam itaque soluta quam perferendis
      et porro consequuntur!`,
    price: 50,
    quantity: 600,
    imagePath: `/photos/1.jpg`

  },
  {
    id: 2,
    name: 'Super fun food2',
    description: `Lorem ipsum dolor sit amet consectetur, adipisicing elit. Beatae
     quibusdam nisi alias laboriosam itaque soluta quam perferendis
      et porro consequuntur!`,
    price: 50,
    quantity: 600,
    imagePath: `${process.env.PUBLIC_URL}/photos/2.jpg`

  },
  {
    id: 3,
    name: 'Super fun food3',
    description: `Lorem ipsum dolor sit amet consectetur, adipisicing elit. Beatae
     quibusdam nisi alias laboriosam itaque soluta quam perferendis
      et porro consequuntur!`,
    price: 50,
    quantity: 600,
    imagePath: `${process.env.PUBLIC_URL}/photos/3.jpg`

  },
  {
    id: 4,
    name: 'Super fun food4',
    description: `Lorem ipsum dolor sit amet consectetur, adipisicing elit. Beatae
     quibusdam nisi alias laboriosam itaque soluta quam perferendis
      et porro consequuntur!`,
    price: 50,
    quantity: 600,
    imagePath: `${process.env.PUBLIC_URL}/photos/4.jpg`

  }
]

export default products;