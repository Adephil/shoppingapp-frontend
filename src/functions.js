export const saveToSessionStorage = (cart) => {
  try {
    const serializedState = JSON.stringify(cart);
    sessionStorage.setItem('cart', serializedState);
  } catch (error) {
    console.log(error);
  }

}

export const loadFromSessionStorage = () => {
  try {
    const cart = JSON.parse(sessionStorage.getItem('cart'));
    console.log(cart)
    return cart || undefined;
  } catch (error) {
    console.log(error);
    return undefined;
  }
}