import React from 'react';

const Loading = (props) => {
  const imgUrl = `${process.env.PUBLIC_URL}/photos/loading_icon.gif`;

  return (
    <div className="loader">
      <img src={imgUrl} alt="loader"/>
    </div>
  )
}


export default Loading;