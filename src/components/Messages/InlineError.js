import React from 'react';
const InlineError = ({ text }) => <span className="text-danger ml-auto">{text}</span>

export default InlineError;