import React from 'react'

class Product extends React.Component {


  render() {
    const { name, price, description, imagePath, cartCount } = this.props.product;
    const imgUrl = `${process.env.PUBLIC_URL}${imagePath}`;

    console.log(this.props.showCounter );
    const button = this.props.showCounter ?
      <div>
        <button className="btn btn-dark" onClick={(e) => {
          e.preventDefault();
          this.props.removeFromCart(this.props.item);
        }}>-</button>
        <span> {cartCount} in cart </span>
        <button className="btn btn-dark" onClick={(e) => {
          e.preventDefault();
          this.props.addToCart(this.props.item);
        }}>+</button>
      </div> :
      <button href="#" className="btn btn-dark block" onClick={(e) => {
        e.preventDefault();
        this.props.addToCart(this.props.product);
      }}>Add to cart</button>


    return (
      <div className="col-md-4 " >
        <div className="card product mt-4" >
          <img className="card-img-top productImage" src={imgUrl} alt="Cardimage" />
          <div className="card-body">
            <h5 className="card-title">{name} <span className="badge badge-warning">&#8358;{price}</span></h5>
            <p className="card-text">{description}</p>
            <button href="#" className="btn btn-dark block" onClick={(e) => {
              e.preventDefault();
              this.props.addToCart(this.props.product);
            }}>Add to cart</button>

          </div>
        </div>
      </div>

    );
  }

}

export default Product;