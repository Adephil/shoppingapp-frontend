import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { removeAllFromCart } from './actions/actions';
import axios from '../../node_modules/axios/dist/axios';
import Loading from './Messages/Loading';




const fontStyle = {
  fontSize: '20px'

};

class OrderList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      orderSuccess: false,
      loading: false
    }
  }
  orderItems = (e) => {
    e.preventDefault();
    const { token, products } = this.props;
    const { loading } = this.state;

    const dbProducts = products.map(product => ({ id: product.id, cartCount: product.cartCount, name: product.name }));

    if (!token) {
      this.props.history.push("/login");
    } else {
      this.setState({ loading: !loading });
      axios.post('/api/order', { products: dbProducts }, {
        headers: { Authorization: "Bearer " + token }
      }).then((response) => {
        this.props.removeAllFromCart();
        this.setState({ loading: false });
        this.setState({ orderSuccess: true });
      })
        .catch(function (error) {
          console.log(error);
        });
    }
  }

  render() {
    const { products } = this.props;
    const { orderSuccess, loading } = this.state;

    if (orderSuccess) {
      return (
        <div className="container text-center mt-4">
          <h2>Thanks for placing an order.</h2>
          <p>You will receive notification shortly.</p>
          <Link to="/products" className="btn btn-dark">Go back Home</Link>
        </div>
      )
    }

    if (loading) {
      return <Loading />
    }

    if (products.length === 0) {
      return (
        <div>
          <h2 className="text-center" style={fontStyle}>No Items to Order <Link to="/products" >Add items here.</Link></h2>
        </div>
      );
    }

    const totalPrice = products.reduce((sum, product) => sum + (product.cartCount * product.price), 0);

    const orderItemTemplate = products.map(product => {
      const { name, cartCount, price, id } = product;
      return (
        <li className="list-group-item bold no-radius" key={id}><span>{cartCount}X {name}</span><span className="pull-right">&#8358;{price * cartCount}</span></li>
      );
    });
    return (
      <div className="container">
        <h2 className="divider">Order Listing</h2>
        <hr />
        <div className="row">
          <div className="col-md-7">
            <div className="panel panel-default mt-4">
              <div className="panel-heading yellocell clearfix px-5 flex">
                <h3 className="panel-title inline-b">Items</h3>
                <Link to="/cart" className="pull-right inline-b bold text-dark underline">Edit</Link>
              </div>
              <div className="panel-body">
                <ul className="list-group">
                  {orderItemTemplate}
                  <li className="yellocell list-group-item"><span className="bold ">Total</span><span className="bold pull-right">&#8358;{totalPrice}</span></li>
                </ul>
              </div>
            </div>
            <p className="mt-4" >
              <button className="btn btn-success" onClick={(e) => {
                e.preventDefault();
                this.orderItems(e);
              }}>Order Items</button>
              <Link className="btn btn-dark ml-3" to="/cart">Back to Cart</Link>
            </p>
          </div>
        </div>
      </div>

    )
  }


}
function mapStateToProps(state) {
  return {
    products: state.cartReducer,
    token: state.userReducer.token
  }
}

export default connect(mapStateToProps, { removeAllFromCart })(OrderList);


