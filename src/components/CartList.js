import React from 'react'
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { addToCart, removeFromCart, removeAllFromCart } from './actions/actions';
import Cart from './Cart';

const fontStyle = {
  fontSize: '20px'

};
const marginStyle = {
  marginTop: '50px',
  marginBottom: '70px'

}

class CartList extends React.Component {

  state = {
    sucessfulOrder: false,
  }
  addToCart = (product) => {

    product.cartCount = product.cartCount ? product.cartCount : 1;
    this.props.addToCart(product);

  }

  removeFromCart = (product) => {
    this.props.removeFromCart(product);
  }

  removeAllFromCart = () => {
    console.log("button clicked");
    this.props.removeAllFromCart();
  }




  render() {
    console.log("from Cart", this.props.products);
    const { products } = this.props;
    if (products.length === 0) {
      return (
        <div>
          <h2 className="text-center" style={fontStyle}>No Items In Cart <Link to="/products" >Add items here.</Link></h2>
        </div>
      );
    }

    const totalPrice = products.reduce((sum, product) => sum + (product.cartCount * product.price), 0);

    const totalQuantity = products.reduce((sum, product) => sum + product.cartCount, 0);

    const productTemplate = products.map(product => {
      return <Cart key={product.id} item={product} addToCart={this.addToCart} removeFromCart={this.removeFromCart} />
    });

    return (
      <div>
        <h2 className="divider">Cart Listing</h2>
        <hr />
        <div className="row">
          <div className="col-md-12">
            <table className="table table-bordered mt-4">
              <thead>
                <tr>
                  <th className="text-center yellocell">Product</th>
                  <th className="text-center yellocell">Quantity</th>
                  <th className="text-center yellocell">Price</th>
                </tr>
              </thead>
              <tbody>
                {productTemplate}
              </tbody>
              <tfoot>
                <tr>
                  <td className="td yellocell"></td>
                  <td className="td yellocell">Total: {totalQuantity} items</td>
                  <td className="td yellocell">Total: &#8358;{totalPrice}</td>
                </tr>
              </tfoot>
            </table>
            <p className="text-center" style={marginStyle}>
              <Link className="btn btn-success mr-3" to="/order">Continue to Order</Link>
              <button className="btn btn-danger" onClick={(e) => {
                e.preventDefault();
                this.removeAllFromCart();
              }}>Delete Cart Items</button>
            </p>

          </div>

        </div>
      </div>
    )
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ addToCart, removeFromCart, removeAllFromCart }, dispatch);
}

function mapStateToProps(state) {
  return {
    products: state.cartReducer,
    token: state.userReducer.token
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CartList);