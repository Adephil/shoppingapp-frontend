import React from 'react'

class Cart extends React.Component {


  render() {
    const { imagePath, name, cartCount, price } = this.props.item;
    const imgUrl = `${process.env.PUBLIC_URL}${imagePath}`;
    return (
      <tr>
        <td className="td"><img src={imgUrl} className="profile" alt="" /> <span>{name}</span></td>
        <td className="td">
          <button className="btn btn-dark" onClick={(e) => {
            e.preventDefault();
            this.props.removeFromCart(this.props.item);
          }}>-</button>
          <span> {cartCount} in cart </span>
          <button className="btn btn-dark" onClick={(e) => {
            e.preventDefault();
            this.props.addToCart(this.props.item);
          }}>+</button>
        </td>
        <td className="td">&#8358;{price * cartCount}</td>
      </tr>
    )
  }
}

export default Cart;