
import React from 'react';
class NavDropDownItem extends React.component {

  render() {
    const {props} = this.props;
    return (
     <Link className="dropdown-item" onClick={props.close} to={props.link}>{props.label}</Link>
    )
  }
}