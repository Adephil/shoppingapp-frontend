import {
  USER_LOGGED_IN,
  USER_LOGGED_OUT
} from '../constants/constants'
import axios from 'axios';



//Actions
export const userLoggedIn = (user) => {
  return {
    type: USER_LOGGED_IN,
    user
  }
}

export const userLoggedOut = () => {
  return {
    type: USER_LOGGED_OUT,
  }
}



// Thunk actions

export const login = (credentials) => (dispatch) => {
  return axios.post('/api/auth/login', credentials)
    .then(response => {
      const {
        user
      } = response.data;
      console.log("user", user);
      localStorage.setItem('user', JSON.stringify(user));
      dispatch(userLoggedIn(user));
    }).catch(err => {
      throw (err);
    });
}

export const logout = () => (dispatch) => {
  console.log('In dispatch ..logout');
  localStorage.removeItem('token');
  dispatch(userLoggedOut());
}