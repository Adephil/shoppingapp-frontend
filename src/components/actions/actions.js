import {
  ADD_TO_CART,
  REMOVE_FROM_CART,
  REMOVE_ALL_FROM_CART
} from '../constants/constants';

export const addToCart = (product) => {
  const action = {
    type: ADD_TO_CART,
    product
  }
  console.log("in action addToCart", action);
  return action;
}

export const removeFromCart = (product) => {
  const action = {
    type: REMOVE_FROM_CART,
    product
  }
  console.log("in action removeFromCart", action);
  return action;
}

export const removeAllFromCart = () => {
  const action = {
    type: REMOVE_ALL_FROM_CART,
  }
  console.log("in action removeAllFromCart", action);
  return action;
}

