import React from 'react';

class NavDropDown extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      toggle: false,
    }
  }

  showDropdown(e) {
    e.preventDefault();
    this.setState({ toggle: !this.state.toggle });
  }
  render() {
    const classDropdownMenu = this.state.toggle ? 'dropdown-menu show' : 'dropdown-menu';
    return (
      <li className="nav-item dropdown">
        <a className="nav-link dropdown-toggle text-white" onClick={(e) => { this.showDropdown(e) }} href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          {this.props.name}
        </a>
        <div className={classDropdownMenu} aria-labelledby="navbarDropdown">
          {this.props.children}
        </div>
      </li>
    );
  }
}

export default NavDropDown;