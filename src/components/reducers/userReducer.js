import { USER_LOGGED_IN, USER_LOGGED_OUT } from '../constants/constants';

const userReducer = (state = {}, action = {}) => {
  const { type, user } = action;
  switch (type) {
    case USER_LOGGED_IN:
      return user;
    case USER_LOGGED_OUT:
      console.log("..logout reducer");
      return {};
    default:
      return state;
  }
}

export default userReducer;