import {
  ADD_TO_CART,
  REMOVE_FROM_CART,
  REMOVE_ALL_FROM_CART
} from '../constants/constants';


const addToCart = (state, product) => {
  const oldProductIndex = state.findIndex(productItem => productItem.id === product.id);
  if (oldProductIndex !== -1) {
    product.cartCount += 1;
    return [
      ...state.slice(0, oldProductIndex),
      product,
      ...state.slice(oldProductIndex + 1)
    ]
  }
  return [...state, product];
}

const removeFromCart = (state, product) => {
  const oldProductIndex = state.findIndex(productItem => productItem.id === product.id);
  if (oldProductIndex !== -1) {
    if (product.cartCount > 1) {
      product.cartCount -= 1;

      return [
        ...state.slice(0, oldProductIndex),
        product,
        ...state.slice(oldProductIndex + 1)
      ]

    } else if (product.cartCount === 1)
      return [
        ...state.slice(0, oldProductIndex),
        ...state.slice(oldProductIndex + 1)
      ]
  }
  return state;
}

const cartReducer = (state = [], action) => {
  let cart = null;
  const {
    type,
    product
  } = action;

  switch (type) {
    case ADD_TO_CART:
      cart = addToCart(state, product);
      console.log("item added  to cart", cart);
      return cart;
    case REMOVE_FROM_CART:
      cart = removeFromCart(state, product);
      console.log("item removed from cart", cart);
      return cart;
    case REMOVE_ALL_FROM_CART:
      console.log("all items removed from cart", cart);
      return [];
    default:
      return state;
  }
}

export default cartReducer;