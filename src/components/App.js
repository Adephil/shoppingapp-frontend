import React from 'react'
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';

import MainLayout from './MainLayout';
import EmptyLayout from './EmptyLayout';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../../node_modules/font-awesome/css/font-awesome.min.css';
import './App.css';
class App extends React.Component {
  render() {
    return (
      <Router>
        <div>
          <Route path="/" component={MainLayout} />
          <Route component={EmptyLayout} />
        </div>
      </Router>
    );
  }
}

export default App;
