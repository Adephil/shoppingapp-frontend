import React from 'react'

import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { login } from './actions/auth';
import Validator from 'validator';
import InlineError from './Messages/InlineError';
import Loading from './Messages/Loading';


class SignIn extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      redirect: false,
      loading: false,
      errors: {}
    }

  }

  validate = (data) => {
    const errors = {}

    if (!Validator.isEmail(data.email)) errors.email = "Invalid Email";
    if (!data.password) errors.password = 'Please Enter a password';

    return errors;
  }
  onSubmit = (e) => {

    e.preventDefault();
    const { redirect, errors, loading, ...formData } = this.state;

    const validationErrors = this.validate(formData);
    this.setState({ errors: validationErrors });


    if (Object.keys(validationErrors).length === 0) {
      this.setState({ loading: !loading });
      this.props.login(formData).then(() => {
        this.props.history.push("/products")
      }).catch(err => {
        this.setState({ loading: false });
        console.log("Error from backend", err);
      });
    }


  }


  render() {
    const { redirect, errors, loading } = this.state;
    console.log(errors);
    if (redirect) {
      return <Redirect to='/products' />
    }

    if (loading) {
      return <Loading />
    }
    return (

      <div className="container">
        <div className="row">
          <div className="col-md-6 offset-md-2 ">
            <form className="mt-5 App " onSubmit={this.onSubmit}>
              <h2 className="text-center">Sign In</h2>

              <div className="form-group">
                <label htmlFor="email">Email address {errors.email && <InlineError text={errors.email} />}</label>
                <input type="email" onChange={event =>
                  this.setState({
                    email: event.target.value,
                  })}
                  onBlur={event =>
                    this.setState({
                      errors: {},
                    })}
                  className="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email" />
              </div>
              <div className="form-group">
                <label htmlFor="exampleInputPassword1">Password {errors.password && <InlineError text={errors.password} />}
                </label>
                <input type="password" onChange={event =>
                  this.setState({
                    password: event.target.value,
                  })}
                  onBlur={event =>
                    this.setState({
                      errors: {},
                    })}
                  className="form-control" id="password" placeholder="Password" />
              </div>
              <button type="submit" className="btn btn-dark">Submit</button>
              <Link rel="stylesheet" className="pull-right btn btn-default" to="/register" >Register</Link>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ login }, dispatch);
}



export default connect(null, mapDispatchToProps)(SignIn);