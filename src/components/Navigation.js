
import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';
class Navigation extends React.Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  render() {

    const { products, isAuthentiated, firstname } = this.props;
    console.log(firstname);
    let count = 0;
    if (products) {
      count = products.reduce((sum, product) => sum + product.cartCount, 0);
    }

    console.log(count);
    return (
      <div>
        <Navbar color="dark" className="mb-4" light expand="md">
          <div className="container">
            <NavbarBrand href="/products" className="text-white">TheRightShop</NavbarBrand>
            <NavbarToggler onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="ml-auto" navbar>
                <NavItem className="text-white">
                  <Link rel="stylesheet" className="nav-link text-white" to="/cart" >
                    Cart <i className="fa fa-shopping-cart "></i><span className="badge badge-warning ml-1">{count}</span> <span className="sr-only">(current)</span>
                  </Link>
                </NavItem>
                <NavItem>
                  <Link rel="stylesheet" className="nav-link text-white" to="/products" >
                    Products
                  </Link>
                </NavItem>
                <NavItem>
                  <Link rel="stylesheet" className="nav-link text-white" to="/register" >
                    <i className="fa fa-shoe "></i> {isAuthentiated ? '' : 'Register'}
                  </Link>
                </NavItem>
                <UncontrolledDropdown nav inNavbar>
                  <DropdownToggle nav caret className="text-white">
                    {isAuthentiated ? firstname : 'Account'}
                  </DropdownToggle>
                  <DropdownMenu right>
                    <DropdownItem>
                      {isAuthentiated ? <Link className="dropdown-item" onClick={(e) => this.props.logout(e)} to="">Log Out</Link> : <Link className="dropdown-item" to="/login">Log In</Link>}
                    </DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
              </Nav>
            </Collapse>
          </div>
        </Navbar>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    products: state.cartReducer,
    isAuthentiated: !!state.userReducer.token,
    firstname: state.userReducer.firstname,

  }
}

// function mapDispatchToProps(dispatch){
//   return bindActionCreators({logout}, dispatch)
// }

export default connect(mapStateToProps, null)(Navigation);

