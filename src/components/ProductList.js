import React from 'react'
import Product from './Product';
import axios from '../../node_modules/axios/dist/axios';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addToCart } from './actions/actions';
import Loading from './Messages/Loading';

class ProductList extends React.Component {

  constructor(props) {
    super(props);


    this.state = {
      loading: false,
      products: [],
    }
  }

  addToCart = (product) => {
    product.cartCount = product.cartCount ? product.cartCount : 1;
    const { createdAt, updatedAt, ...stateProduct } = product;
    this.props.addToCart(stateProduct);
  }

  componentDidMount() {
    const { loading } = this.state;
    this.setState({ loading: !loading });
    axios.get('/api/products/')
      .then((response) => {
        this.setState({ products: response.data.data });
        this.setState({ loading: false });
      })
      .catch(function (error) {
        // this.setState({ loading: false });
        console.log(error);
      });
  }



  render() {
    const { loading } = this.state;
    const productItems = this.state.products.map(product => {
      return (
        <Product product={product} key={product.id} addToCart={this.addToCart} />
      )
    });

    if (loading) {
      return <Loading />
    }
    return (

      <div>
        <h2 className="divider">Product Listing</h2>
        <hr />
        <div className="row">
          {productItems}
        </div>
      </div>

    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ addToCart }, dispatch);
}

function mapStateToProps(state) {
  return {
    products: state.cartReducer
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);
