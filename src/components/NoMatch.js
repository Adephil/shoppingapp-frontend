import React from 'react';
import { Link } from 'react-router-dom';
const NoMatch = () => {

  return (
    <div className="404">
      <h3>404</h3>
      <p>Sorry this page doesn't exist <Link to="/products" /></p>
    </div>
  )
}

export default NoMatch;