import React, { Component } from 'react';
import { BrowserRouter as  Route } from 'react-router-dom';

import SignUp from './SignUp';
import SignIn from './SignIn';
class EmptyLayout extends Component {
  render() {
    return (
      <div className="container">
        <Route path='/register' component={SignUp} />
        <Route path='/login' component={SignIn} />
        {/* <Route component={NoMatch} /> */}

      </div>
    );
  }
}
export default EmptyLayout;