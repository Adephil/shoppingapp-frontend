import React, { Component } from 'react';
import Navigation from './Navigation';
import CartList from './CartList';
import ProductList from './ProductList';
import OrderList from './OrderList';
import { connect } from 'react-redux';
import { logout } from './actions/auth';
import { removeAllFromCart } from './actions/actions';

import {
  Route
} from 'react-router-dom';
class MainLayout extends Component {

  logout = (e) => {
    e.preventDefault();

    this.props.logout();
    this.props.removeAllFromCart();
    this.props.history.push("/login");
  }

  render() {
    return (
      <div>
        <Navigation logout={this.logout} />
        <div className="container">
          <Route path='/products' component={ProductList} />
          <Route path="/cart" component={CartList} />
          <Route path="/order" component={OrderList} />
        </div>
      </div>
    );
  }
}


export default connect(null, { logout, removeAllFromCart })(MainLayout);