import React from 'react'
import axios from '../../node_modules/axios/dist/axios';
import { Redirect } from 'react-router-dom';
import Validator from 'validator';
import { PHONE_NUMBER_LENGTH } from '../components/constants/constants';
import InlineError from './Messages/InlineError.js';
import { Link } from 'react-router-dom';
import Loading from './Messages/Loading';
class SignUp extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      firstname: '',
      lastname: '',
      email: '',
      phonenumber: '',
      password: '',
      redirect: false,
      errors: {}
    }
  }

  validate = (data) => {
    const errors = {}
    const { email, phonenumber, password, firstname, lastname } = data;
    if (!Validator.isEmail(email)) errors.email = "Invalid Email";
    if (!firstname || !Validator.isAlpha(firstname)) errors.firstname = 'Invalid Firstname';
    if (!lastname || !Validator.isAlpha(lastname)) errors.lastname = 'Invalid Lastname';
    if (phonenumber.length < PHONE_NUMBER_LENGTH) errors.phonenumber = `PhoneNumber should be ${PHONE_NUMBER_LENGTH}`;
    if (!phonenumber || !Validator.isNumeric(phonenumber)) errors.phonenumber = 'PhoneNumber should contain only numbers';
    if (!password) errors.password = 'Invalid Password';

    return errors;
  }

  onSubmit = (e) => {
    e.preventDefault();
    const { redirect, errors, loading, ...formData } = this.state;
    console.log(formData);
    const validationErrors = this.validate(formData);
    this.setState({ errors: validationErrors })

    if (Object.keys(validationErrors).length === 0) {
      this.setState({ loading: !loading });
      axios.post('/api/register', formData)
        .then((response) => {
          this.setState({ redirect: true });
        })
        .catch(function (error) {
          this.setState({ loading: false });
          console.log(error);
        });
    }

  }


  render() {
    const { redirect, loading, errors } = this.state;
    if (redirect) {
      return <Redirect to='/login' />
    }
    if (loading) {
      return <Loading />
    }
    return (

      <div className="container">
        <div className="row">
          <div className="col-md-6 offset-md-2 ">
            <form className="mt-5 App " onSubmit={this.onSubmit}>
              <h2 className="text-center">Register</h2>

              <div className="form-group">
                <label htmlFor="firstname">First Name {errors.firstname && <InlineError text={errors.firstname} />}</label>
                <input type="text" onChange={event =>
                  this.setState({
                    firstname: event.target.value,
                  })}
                  onBlur={event =>
                    this.setState({
                      errors: {},
                    })}
                  className="form-control" id="firstname" aria-describedby="emailHelp" placeholder="First name" />
              </div>
              <div className="form-group">
                <label htmlFor="lastname">Last Name {errors.lastname && <InlineError text={errors.lastname} />}</label>
                <input type="text" onChange={event =>
                  this.setState({
                    lastname: event.target.value,
                  })}
                  onBlur={event =>
                    this.setState({
                      errors: {},
                    })}
                  className="form-control" id="lastname" aria-describedby="emailHelp" placeholder="last name" />
              </div>
              <div className="form-group">
                <label htmlFor="phonenumber">Phone Number {errors.phonenumber && <InlineError text={errors.phonenumber} />}</label>
                <input type="text" onChange={event =>
                  this.setState({
                    phonenumber: event.target.value,
                  })}
                  onBlur={event =>
                    this.setState({
                      errors: {},
                    })}
                  className="form-control" id="phonenumber" aria-describedby="emailHelp" placeholder="phone number" />
              </div>
              <div className="form-group">
                <label htmlFor="email">Email address {errors.email && <InlineError text={errors.email} />}</label>
                <input type="email" onChange={event =>
                  this.setState({
                    email: event.target.value,
                  })}
                  onBlur={event =>
                    this.setState({
                      errors: {},
                    })}
                  className="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email" />
              </div>
              <div className="form-group">
                <label htmlFor="password">Password {errors.password && <InlineError text={errors.password} />}</label>
                <input type="password" onChange={event =>
                  this.setState({
                    password: event.target.value,
                  })}
                  onBlur={event =>
                    this.setState({
                      errors: {},
                    })}
                  className="form-control" id="password" placeholder="Password" />
              </div>
              <button type="submit" className="btn btn-dark ">Submit</button>
              <Link rel="stylesheet" className="pull-right btn btn-default" to="/login" >Login</Link>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

export default SignUp;